﻿using System;
using System.Windows;
using Kompas6Constants;
using Kompas6API5;
using Kompas6Constants3D;
using Excel = Microsoft.Office.Interop.Excel;
using System.Diagnostics;





namespace WpfApplication1
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public KompasObject kompas;
        public Excel.Application excelApp; // Это сам процесс, а дальше вниз по иерархии спускаемся
        public Excel.Workbooks excelBooks; // Все эти переменные должны быть инициализированы именно здесь, в глобальных параметрах; ибо инициализация объекта в самой функции может привести к тому, что при повторном вызове функции сгенерируется системное исключение System.COM.Exception
        public Excel.Workbook excelBook;
        public Excel.Worksheet excelSheet; // Один лист
        public Excel.Range excelRange; // А это хреня, считывающая данные; см. описание структуры документа Excel
        public Excel.Range excelRange2;
        public ksDocument2D iDocument2D; public ksStamp stamp;
        public ksStamp stamp2;
        public ksSpcDocument iSpcDocument;
        public class RadioButton : System.Windows.Controls.Primitives.ToggleButton { };


        public MainWindow()
        {

            InitializeComponent();
            closeButton.IsEnabled = false;
            button.IsEnabled = false;
            radioButton.IsEnabled = false;
            radioButton1.IsEnabled = false;
            radioButton.IsChecked = true;
        }

        private void openbutton_Click(object sender, RoutedEventArgs e)
        {
            Process[] proceses = Process.GetProcessesByName("KOMPAS");
            foreach (Process process in proceses)
            {
                process.CloseMainWindow();
                process.Kill();
            }
            /* if (kompas != null)
             {
                 kompas.Visible = true;
                 kompas.ActivateControllerAPI();
               //  radioButton.IsEnabled = true;
                // openbutton.IsEnabled = false;
                 //closeButton.IsEnabled = true;
                 //button.IsEnabled = true;
                 //radioButton1.IsEnabled = true;
             }*/
            if (kompas == null)
            {
                if (radioButton.IsEnabled == false || openbutton.IsEnabled == true || closeButton.IsEnabled == false || button.IsEnabled == false ||
              radioButton1.IsEnabled == false)
                {
                    radioButton.IsEnabled = true;
                    openbutton.IsEnabled = false;
                    closeButton.IsEnabled = true;
                    button.IsEnabled = true;
                    radioButton1.IsEnabled = true;
                }
                Type t = Type.GetTypeFromProgID("KOMPAS.Application.5");
                object processKompas = (KompasObject)Activator.CreateInstance(t);
                kompas = (KompasObject)processKompas;
                kompas.Visible = true;
                //kompasisalive(true);

            }

        }


        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            Process[] proceses = Process.GetProcessesByName("KOMPAS");
            foreach (Process process in proceses)
            {
                process.CloseMainWindow();
                process.Kill();
            }
            //   if (openbutton.IsEnabled == false || closeButton.IsEnabled == true ||  button.IsEnabled == true || radioButton.IsEnabled== true || 
            //    radioButton1.IsEnabled==true )
            //        {

            //   }
            if (kompas != null)
            {
                kompas = null;
                closeButton.IsEnabled = false;
                openbutton.IsEnabled = true;
                button.IsEnabled = false;
                radioButton1.IsEnabled = false;
                radioButton.IsEnabled = false;
            }

            //  if (kompas == null)
            // {
            //    MessageBox.Show("Компасс не запущен", "WARNING", MessageBoxButton.OK, MessageBoxImage.Warning);
            //}

        }


        string name_blobal;
        //  Выбор имени файла
        void FileNameSelect()
        {
            string name = kompas.ksChoiceFile("cdw", null, true);
            if (name != string.Empty)
                kompas.ksMessage(name);
            else
                kompas.ksMessage("отказ");
            name_blobal = name;
        }




        private void button_Click(object sender, RoutedEventArgs e)
        {

            bool allIsOkInXls = true;
            bool xlsExport = false;
            Microsoft.Win32.OpenFileDialog openXlsFileDialog = new Microsoft.Win32.OpenFileDialog();

            if (!(xlsExport))
            {

                //openXlsFileDialog.ToString();
                openXlsFileDialog.Title = "Выберите пресет для импорта данных";
                openXlsFileDialog.Filter = "Книга Excel 2003 (*.xls)|*.xls|Книга Excel 2007 (*.xlsx)|*.xlsx";
                openXlsFileDialog.FilterIndex = 2;
                Nullable<bool> result = openXlsFileDialog.ShowDialog();
                if (!((bool)result))
                {

                    allIsOkInXls = false;
                }
                // Выход, если была нажата кнопка Отмена и прочие (кроме ОК)

            }
            if (kompas == null)
            {
                closeButton.IsEnabled = false;
                openbutton.IsEnabled = true;
                button.IsEnabled = false;
                radioButton1.IsEnabled = false;
                radioButton.IsEnabled = false;
                MessageBox.Show("Компасс не запущен", "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else
                try
                {
                    ksDocumentParam iDocumentParam = (ksDocumentParam)kompas.GetParamStruct((short)StructType2DEnum.ko_DocumentParam);
                    if (iDocumentParam != null)
                    {
                        iDocumentParam.Init();
                        if ((bool)radioButton.IsChecked == true) // Если выбран чертёж
                        {
                            iDocumentParam.type = 1; // Из перечисления DocType, 1 = чертеж, 4 = спецификация
                        }
                        else if ((bool)radioButton1.IsChecked == true) // Если выбрана спецификация
                        {
                            iDocumentParam.type = 4;
                        }
                        iDocumentParam.regime = 0; // Режим редактирования: 0 = видимый, 1 = "слепой"
                        ksSheetPar iSheetParam = (ksSheetPar)iDocumentParam.GetLayoutParam(); // Оформление
                        iSheetParam.Init();
                        iSheetParam.layoutName = kompas.ksSystemPath(0) + "/GRAPHIC.LYT"; //  Имя библиотеки стилей спецификации

                        ksStandartSheet iStandartSheet = (ksStandartSheet)iSheetParam.GetSheetParam();



                        if (allIsOkInXls)
                        {
                            // На запуск уходит от 1 до 10 секунд, не торопись
                            excelApp = new Excel.Application();
                            if (!(xlsExport)) // Если импорт
                            {
                                excelApp.Workbooks.Open(openXlsFileDialog.FileName);
                                // замутить проверку ска                                                         !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                                excelBook = excelApp.ActiveWorkbook;
                            }

                            excelSheet = (Excel.Worksheet)excelBook.Worksheets[1];


                            String[,] xlsDocumentArray = new string[5, 11]; // Три раздела по десять свойств, не считая нулей; 1=5, 2=10, 3=8 и ещё одно для масштаба, итого 24 свойства
                            if (!(xlsExport)) // Если импорт, то все данные считываются с документа
                            {
                                int j = 1; int k = 1;
                                for (int i = 2; i < 25; i++)
                                {
                                    excelRange = excelSheet.get_Range("B" + i.ToString());
                                    xlsDocumentArray[j, k] = excelRange.Text;
                                    k++;
                                    if ((j == 1) && (k > 5)) { j++; k = 1; };
                                    if ((j == 2) && (k > 10)) { j++; k = 1; };
                                    if ((j == 3) && (k == 7)) // Здесь будет лишь доп. цифра, скреплённая с масштабом
                                    {
                                        excelRange2 = excelSheet.get_Range("C" + i.ToString());
                                        xlsDocumentArray[4, 1] = excelRange2.Text;
                                    };
                                    if ((j == 3) && (k > 8)) { j = 1; k = 1; };
                                }
                            }





                            if (allIsOkInXls)
                            {
                                if (!(xlsExport)) // Если импорт
                                {
                                    bool gostStr = stringCheck(xlsDocumentArray[3, 1], "StringsWithAll");
                                    if (!(gostStr))
                                    {
                                        allIsOkInXls = false;
                                        MessageBox.Show("Ошибка в поле 'ГОСТ", "Ошибка в ГОСТ", MessageBoxButton.OK, MessageBoxImage.Warning);
                                    }
                                }

                            }

                            // -----------------
                            // Проверка на формат
                            if (allIsOkInXls)
                            {
                                if (!(xlsExport)) // Если импорт
                                {
                                    int sheetFormatChar = xlsDocumentArray[1, 1][0];
                                    int sheetFormatNumber = xlsDocumentArray[1, 1][1];

                                    if (((sheetFormatNumber >= 48) && (sheetFormatNumber <= 53)) || ((sheetFormatChar == 1040) || (sheetFormatChar == 65)))
                                    {
                                        // ASCII-коды чисел от 0 до 5, также char, обозначающий (рус/англ) букву "А"
                                        // Т.е. либо ошибка в цифрах, либо не найдена буква "А"
                                        // Число 5 нах. на 53 месте, разница между ними по таблице ASCII = 48
                                        iStandartSheet.format = (short)(sheetFormatNumber - 48);
                                        iStandartSheet.direct = false;
                                    }
                                    else
                                    {
                                        allIsOkInXls = false;
                                        MessageBox.Show("Неправильно задан формат чертежа", "Ошибка во вводе формата", MessageBoxButton.OK, MessageBoxImage.Warning);
                                    }
                                }
                            }

                            // -----------------
                            // Проверка блока доп. инфы: Разраб, Принял итц.
                            if (allIsOkInXls) // Если что-то в файле неправильно - файл просто не будет дальше считываться
                            {
                                if (!(xlsExport)) // Если импорт
                                {
                                    bool b1 = stringCheck(xlsDocumentArray[2, 1], "Strings");
                                    bool b2 = stringCheck(xlsDocumentArray[2, 2], "Strings");
                                    bool b3 = stringCheck(xlsDocumentArray[2, 3], "Strings");
                                    bool b4 = stringCheck(xlsDocumentArray[2, 4], "Strings");
                                    bool b5 = stringCheck(xlsDocumentArray[2, 5], "Strings");

                                    if (b1 && b2 && b3 && b4 && b5) { allIsOkInXls = true; }
                                    else
                                    {
                                        MessageBox.Show("Обнаружены недопустимые символы в поле 'Доп. информация';\n допустимые для ввода: [а-я А-Я a-z A-Z пробел ' " + '"' + " ]", "Ошибка во вводе строки", MessageBoxButton.OK, MessageBoxImage.Warning);
                                        allIsOkInXls = false;
                                    }
                                }
                            }


                            // -----------------
                            // Проверка количества
                            if (allIsOkInXls)
                            {
                                if (!(xlsExport)) // Если импорт
                                {
                                    bool bAmount = stringCheck(xlsDocumentArray[3, 4], "Numbers");
                                    if (bAmount && (xlsDocumentArray[3, 4].Length < 3)) // Не думаю, что количество будет больше 99
                                    {
                                        allIsOkInXls = true;
                                    }
                                    else
                                    {
                                        MessageBox.Show("Обнаружены недопустимые символы, требуется ввод цифр", "Ошибка в блоке 'Количество'", MessageBoxButton.OK, MessageBoxImage.Warning);
                                        allIsOkInXls = false;
                                    }
                                }
                            }

                            // -----------------
                            // Проверка массы
                            if (allIsOkInXls)
                            {
                                if (!(xlsExport)) // Если импорт
                                {
                                    bool bAmount = stringCheck(xlsDocumentArray[3, 5], "NumbersWithPoint"); // Масса может содержать одну запятую
                                    if (bAmount && (xlsDocumentArray[3, 5].Length < 11)) // Максимум 10 символов, включая запятую
                                    {
                                        allIsOkInXls = true;
                                    }
                                    else
                                    {
                                        MessageBox.Show("Обнаружены недопустимые символы, требуется ввод цифр", "Ошибка в блоке 'Масса'", MessageBoxButton.OK, MessageBoxImage.Warning);
                                        allIsOkInXls = false;
                                    }
                                }
                            }
                            // -----------------
                            // Проверка масштаба
                            if (allIsOkInXls)
                            {
                                if (!(xlsExport)) // Если импорт
                                {
                                    bool bMashtab = stringCheck(xlsDocumentArray[3, 6] + ":" + xlsDocumentArray[4, 1], "NumbersWithColon"); // Масштаб x:y, где x, y - соотв. числа из файла
                                    bool bMashtabNorm = (!(xlsDocumentArray[3, 6] == "0")) && (!(xlsDocumentArray[3, 6] == "00")) && (!(xlsDocumentArray[4, 1] == "0")) && (!(xlsDocumentArray[4, 1] == "00")) && (xlsDocumentArray[3, 6].Length < 3) && (xlsDocumentArray[3, 6].Length < 3);
                                    if (bMashtab && bMashtabNorm) // Так вот эти вот x и y из коммента выше не должны быть больше 99, также не должны быть равны нулю
                                    {
                                        allIsOkInXls = true;
                                    }
                                    else
                                    {
                                        MessageBox.Show("Обнаружены недопустимые символы, требуется ввод цифр и максимум одного двоеточия", "Ошибка в блоке 'Масштаб'", MessageBoxButton.OK, MessageBoxImage.Warning);
                                        allIsOkInXls = false;
                                    }
                                }
                            }
                            // ----------------
                            // Проверка дат
                            if (allIsOkInXls)
                            {
                                if (!(xlsExport)) // Если импорт
                                {
                                    bool bDate1 = (stringCheck(xlsDocumentArray[2, 6], "Date")) && dateCheck(xlsDocumentArray[2, 6]) && (xlsDocumentArray[2, 6].Length < 11);
                                    bool bDate2 = (stringCheck(xlsDocumentArray[2, 7], "Date")) && dateCheck(xlsDocumentArray[2, 7]) && (xlsDocumentArray[2, 7].Length < 11);
                                    bool bDate3 = (stringCheck(xlsDocumentArray[2, 8], "Date")) && dateCheck(xlsDocumentArray[2, 8]) && (xlsDocumentArray[2, 8].Length < 11);
                                    bool bDate4 = (stringCheck(xlsDocumentArray[2, 9], "Date")) && dateCheck(xlsDocumentArray[2, 9]) && (xlsDocumentArray[2, 9].Length < 11);
                                    bool bDate5 = (stringCheck(xlsDocumentArray[2, 10], "Date")) && dateCheck(xlsDocumentArray[2, 10]) && (xlsDocumentArray[2, 10].Length < 11);

                                    if (bDate1 && bDate2 && bDate3 && bDate4 && bDate5)
                                    {
                                        allIsOkInXls = true;
                                    }
                                    else { MessageBox.Show("Неверный формат даты", "Ошибка в дате", MessageBoxButton.OK, MessageBoxImage.Warning); allIsOkInXls = false; }
                                } // Если экспорт, то и проверять нечего, это ж даты с формы
                            }

                            // --------------
                            // Проверка ещё 7 параметров чисто на текст, см. шаблон на параметры B4-B6, B18-B19, B23-B24
                            if (allIsOkInXls)
                            {
                                if (!(xlsExport)) // Если импорт
                                {
                                    bool bString1 = (stringCheck(xlsDocumentArray[3, 2], "StringsWithAll"));
                                    bool bString2 = (stringCheck(xlsDocumentArray[3, 3], "StringsWithAll"));
                                    bool bString3 = (stringCheck(xlsDocumentArray[1, 5], "StringsWithAll"));
                                    bool bString4 = (stringCheck(xlsDocumentArray[1, 3], "StringsWithAll"));
                                    bool bString5 = (stringCheck(xlsDocumentArray[1, 4], "StringsWithAll"));
                                    bool bString6 = (stringCheck(xlsDocumentArray[3, 7], "StringsWithAll"));
                                    bool bString7 = (stringCheck(xlsDocumentArray[3, 8], "StringsWithAll"));
                                    if (bString1 && bString2 && bString3 && bString4 && bString5 && bString6 && bString7)
                                    {
                                        allIsOkInXls = true;
                                    }
                                    else
                                    {
                                        if (!(bString1))
                                        {
                                            MessageBox.Show("Обнаружены недопустимые символы; допустимые для ввода: [а-я А-Я a-z A-Z пробел ' " + '"' + " ]", "Ошибка в блоке 'Наименование'(Детали)", MessageBoxButton.OK, MessageBoxImage.Warning);
                                        }
                                        if (!(bString2))
                                        {
                                            MessageBox.Show("Обнаружены недопустимые символы; допустимые для ввода: [а-я А-Я a-z A-Z пробел ' " + '"' + " ]", "Ошибка в блоке 'Примечание'(Детали)", MessageBoxButton.OK, MessageBoxImage.Warning);
                                        }
                                        if (!(bString3))
                                        {
                                            MessageBox.Show("Обнаружены недопустимые символы; допустимые для ввода: [а-я А-Я a-z A-Z пробел ' " + '"' + " ]", "Ошибка в блоке 'Предприятие-изготовитель'(Детали)", MessageBoxButton.OK, MessageBoxImage.Warning);
                                        }
                                        if (!(bString4))
                                        {
                                            MessageBox.Show("Обнаружены недопустимые символы; допустимые для ввода: [а-я А-Я a-z A-Z пробел ' " + '"' + " ]", "Ошибка в блоке 'Наименование'(Документация)", MessageBoxButton.OK, MessageBoxImage.Warning);
                                        }
                                        if (!(bString5))
                                        {
                                            MessageBox.Show("Обнаружены недопустимые символы; допустимые для ввода: [а-я А-Я a-z A-Z пробел ' " + '"' + " ]", "Ошибка в блоке 'Примечание'(Документация)", MessageBoxButton.OK, MessageBoxImage.Warning);
                                        }
                                        if (!(bString6))
                                        {
                                            MessageBox.Show("Обнаружены недопустимые символы; допустимые для ввода: [а-я А-Я a-z A-Z пробел ' " + '"' + " ]", "Ошибка в блоке 'Материал'(Детали)", MessageBoxButton.OK, MessageBoxImage.Warning);
                                        }
                                        if (!(bString7))
                                        {
                                            MessageBox.Show("Обнаружены недопустимые символы; допустимые для ввода: [а-я А-Я a-z A-Z пробел ' " + '"' + " ]", "Ошибка в блоке 'Пользовательская'(Детали)", MessageBoxButton.OK, MessageBoxImage.Warning);
                                        }
                                        allIsOkInXls = false;

                                    }

                                    //===================================================================================================================================
                                    if ((bool)radioButton.IsChecked == true) // Если выбран чертёж
                                    {
                                        FileNameSelect();
                                        ksDocument2D iDocument2D = (ksDocument2D)kompas.Document2D();
                                        //     iDocument2D.ksCreateDocument(iDocumentParam);
                                        ksStamp stamp = (ksStamp)iDocument2D.GetStamp();
                                        bool back_result = iDocument2D.ksOpenDocument(name_blobal, false);
                                        iDocument2D.ksCircle(10, 10, 10, 0);
                                        iDocument2D.ksLineSeg(0, 40, 100, 40, 1);
                                        StampForDoc(iDocument2D, null, stamp, xlsDocumentArray);



                                    }
                                    else if ((bool)radioButton1.IsChecked == true) // Если выбрана спецификация
                                    {
                                        ksSpcDocument iSpcDocument = (ksSpcDocument)kompas.SpcDocument();
                                        iSpcDocument.ksCreateDocument(iDocumentParam);
                                        ksStamp stamp2 = (ksStamp)iSpcDocument.GetStamp();
                                        // Документ спецификации
                                        StampForDoc(null, iSpcDocument, stamp2, xlsDocumentArray);
                                    }
                                    else
                                    {
                                        MessageBox.Show("RadioButtonNotClicked", "не выбрано условие", MessageBoxButton.OK, MessageBoxImage.Error);
                                    }
                                }

                            }
                        }
                    }
                    Process[] proceses = Process.GetProcessesByName("EXCEL");
                    foreach (Process process in proceses)
                    {
                        process.CloseMainWindow();
                        process.Kill();
                    }
                }



                catch
                {
                    /* Что бы ни произошло, придётся закрывать процесс экселя, иначе
                       процесс висеть в системе останется, и из-за этого 
                       могут быть ошибки при записи в файл */
                    if (excelApp != null)
                    {
                        if (excelBook != null)
                        {
                            excelBook.Save();
                            excelApp.Quit();
                        }
                    }
                    MessageBox.Show("PluginError", "SAS", MessageBoxButton.OK, MessageBoxImage.Error);
                }
        }


        public void PushCircle(double x, double y, double rad, double depth, string plane, string orientation)
        {

            var doc = (ksDocument3D)kompas.ActiveDocument3D();
            var part = (ksPart)doc.GetPart((short)Part_Type.pNew_Part);
            if (part == null) return;
            // Создаем новый эскиз
            var entitySketch = (ksEntity)part.NewEntity((short)Obj3dType.o3d_sketch);
            if (entitySketch != null)
            {
                // интерфейс свойств эскиза
                var sketchDef = (ksSketchDefinition)entitySketch.GetDefinition();
                if (sketchDef != null)
                {
                    // получим интерфейс базовой плоскости
                    ksEntity basePlane;
                    if (plane == "XOY")
                    {
                        basePlane = (ksEntity)part.GetDefaultEntity((short)Obj3dType.o3d_planeXOY);
                    }
                    else
                    {
                        basePlane = (ksEntity)part.GetDefaultEntity((short)Obj3dType.o3d_planeXOZ);
                    }
                    sketchDef.SetPlane(basePlane);	// установим плоскость базовой для эскиза
                    entitySketch.Create();			// создадим эскиз1

                    // интерфейс редактора эскиза
                    var sketchEdit = (ksDocument2D)sketchDef.BeginEdit();

                    //круглое отверстие
                    sketchEdit.ksCircle(x, y, rad, 1);
                    sketchDef.EndEdit();    // завершение редактирования эскиза



                }
            }
        }
        public void StampForDoc(ksDocument2D document2D, ksSpcDocument SpcDocument, ksStamp stamp, string[,] xlsDocumentArray)
        {
            if (document2D == null)
            {
                stamp = (ksStamp)SpcDocument.GetStamp();
            }
            else if (SpcDocument == null)
            {
                stamp = (ksStamp)document2D.GetStamp();
            }

            if (SpcDocument != null) // Если это спецификация, то необходимо её сгенерировать, если есть необходимость
            {
                if (xlsDocumentArray != null)
                {
                    ksSpecification iSpc = (ksSpecification)SpcDocument.GetSpecification(); // Сама спецификация

                    iSpc.ksSpcObjectCreate("", 1, 5, 0, 0, 0); // Номер раздела = 5, для документации
                    iSpc.ksSetSpcObjectColumnText(1, 1, 0, xlsDocumentArray[1, 1]);
                    iSpc.ksSetSpcObjectColumnText(4, 1, 0, xlsDocumentArray[1, 2]);
                    iSpc.ksSetSpcObjectColumnText(5, 1, 0, xlsDocumentArray[1, 3]);
                    iSpc.ksSetSpcObjectColumnText(6, 1, 0, "1");
                    iSpc.ksSetSpcObjectColumnText(7, 1, 0, xlsDocumentArray[1, 4]);
                    iSpc.ksSpcObjectEnd();

                    for (int i = 1; i < 2; i++)
                    {
                        iSpc.ksSpcObjectCreate("", 1, 20, 0, 0, 1); // Номер раздела = 20, для блока "Детали"
                        iSpc.ksSetSpcObjectColumnText(1, 1, 0, xlsDocumentArray[1, 1]);
                        iSpc.ksSetSpcObjectColumnText(3, 1, 0, i.ToString());
                        iSpc.ksSetSpcObjectColumnText(4, 1, 0, "ГОСТ " + xlsDocumentArray[3, 1]);
                        iSpc.ksSetSpcObjectColumnText(5, 1, 0, xlsDocumentArray[3, 2]);
                        iSpc.ksSetSpcObjectColumnText(6, 1, 0, xlsDocumentArray[3, 4]);
                        iSpc.ksSetSpcObjectColumnText(7, 1, 0, xlsDocumentArray[3, 3]);
                        iSpc.ksSetSpcObjectColumnText(8, 1, 0, xlsDocumentArray[3, 5]);
                        iSpc.ksSetSpcObjectColumnText(9, 1, 0, xlsDocumentArray[3, 7]);
                        iSpc.ksSetSpcObjectColumnText(10, 1, 0, xlsDocumentArray[3, 8]);
                        iSpc.ksSetSpcObjectColumnText(12, 1, 0, xlsDocumentArray[1, 5]);
                        iSpc.ksSpcObjectEnd();
                    }

                }
            }

            ksTextItemParam textItemParam = (ksTextItemParam)kompas.GetParamStruct((short)StructType2DEnum.ko_TextItemParam);
            stamp.ksOpenStamp();
            setStamp(1, xlsDocumentArray[3, 2], textItemParam, stamp); // Наименование (детали)
            setStamp(2, xlsDocumentArray[1, 2], textItemParam, stamp); // Обозначение (документация)
            setStamp(3, xlsDocumentArray[3, 7], textItemParam, stamp); // Материал (детали)
            setStamp(5, xlsDocumentArray[3, 5], textItemParam, stamp); // Масса (детали)
            setStamp(6, xlsDocumentArray[3, 6], textItemParam, stamp); // Масштаб (детали)
            setStamp(8, 1.ToString(), textItemParam, stamp); // Листов
            setStamp(9, xlsDocumentArray[1, 5], textItemParam, stamp); // Предприятие-изготовитель (документация)
            setStamp(32, xlsDocumentArray[1, 1], textItemParam, stamp); // Формат (документация)
            setStamp(110, xlsDocumentArray[2, 1], textItemParam, stamp); // Разраб (доп.инфа)
            setStamp(111, xlsDocumentArray[2, 2], textItemParam, stamp); // Пров. (доп.инфа)
            setStamp(112, xlsDocumentArray[2, 3], textItemParam, stamp); // Т.контр. (доп.инфа)
            setStamp(114, xlsDocumentArray[2, 4], textItemParam, stamp); // Н.контр. (доп.инфа)
            setStamp(115, xlsDocumentArray[2, 5], textItemParam, stamp); // Утв. (доп.инфа)
            setStamp(130, xlsDocumentArray[2, 6], textItemParam, stamp); // Дата разраба (доп.инфа)
            setStamp(131, xlsDocumentArray[2, 7], textItemParam, stamp); // Дата проверки (доп.инфа)
            setStamp(132, xlsDocumentArray[2, 8], textItemParam, stamp); // Дата нормоконтроля (доп.инфа)
            setStamp(134, xlsDocumentArray[2, 9], textItemParam, stamp); // Дата нормоконтроля (доп.инфа)
            setStamp(135, xlsDocumentArray[2, 10], textItemParam, stamp); // Дата утверждения (доп.инфа)
            stamp.ksCloseStamp();


        }
        public void setStamp(int columnNumber, string tempStr, ksTextItemParam textItemParam, ksStamp tempStamp)
        {
            tempStamp.ksColumnNumber(columnNumber);
            textItemParam.s = tempStr;
            tempStamp.ksTextLine(textItemParam);
        }
        public Boolean stringCheck(string temp, string regime)
        {
            bool allIsOkHere = true;
            int symbolFoundTwice = 0;

            // Код сильно дублируется, однако у меня уже времени нет всё это оптимизировать; работает и ладно
            for (int i = 0; i < temp.Length; i++) // Читает первые сто символов с имён лиц, проверяющих документ: Разраб, Проверил итц.
            {
                /*  Числа: 0-9[48-57]
                    Пробел: 32, Точка: 46, Двоеточие: 58
                    Русский алфавит: а-я[1072-1103]; А-Я[1040-1071]
                    Английский алфавит: a-z[97-122]; A-Z[65-90] */
                char tempChar = temp[i];
                bool tempCharIsLast = (i == temp.Length - 1);
                bool tempBoolRu = ((tempChar >= 1072) && (tempChar <= 1103)) || ((tempChar >= 1040) && (tempChar <= 1171)); // а-я либо А-Я
                bool tempBoolEn = ((tempChar >= 97) && (tempChar <= 122)) || ((tempChar >= 65) && (tempChar <= 90)); // a-z либо A-Z
                bool tempBoolNumbers = ((tempChar >= 48) && (tempChar <= 57)); // 0-9
                bool tempBoolComma = (tempChar == 44); // Ну тогда будет запятая вместо точки, ибо Excel конвертит значения с точкой в дату >_<
                bool tempBoolColon = (tempChar == 58); // Двоеточие
                bool tempBoolSpace = (tempChar == 32); // Пробел
                bool tempBoolPoint = (tempChar == 46); // Точка
                bool tempQuotationMark = (tempChar == 39); // Апостроф одинарный (')
                bool tempDoubleQuotationMark = (tempChar == 34); // Апостроф двойной (")

                switch (regime)
                {
                    // Цифры в конце переменных потому, что в каждом кейсе нужно свои отдельные переменные определять >_<
                    case "Strings":
                        if (tempBoolRu || tempBoolEn || tempBoolSpace || tempBoolPoint) // Из допустимых только рус\англ\пробел\точка
                        {
                            allIsOkHere = true; // Всё норм
                        }
                        else
                        {
                            allIsOkHere = false; // "Ой всё" ©
                            break;
                        }
                        break;
                    case "Numbers":
                        if (tempBoolNumbers) // Из допустимых только цифры
                        {
                            allIsOkHere = true;
                        }
                        else
                        {
                            allIsOkHere = false;
                            break;
                        }
                        break;
                    case "StringsWithNumbers":
                        if (tempBoolRu || tempBoolEn || tempBoolSpace || tempBoolPoint || tempBoolNumbers) // Из допустимых только рус\англ\пробел\точка\цифры
                        {
                            allIsOkHere = true; // Всё норм
                        }
                        else
                        {
                            allIsOkHere = false; // "Ой всё" ©
                            break;
                        }
                        break;
                    case "StringsWithAll":
                        if (tempBoolRu || tempBoolEn || tempBoolNumbers || tempBoolComma
                            || tempBoolColon || tempBoolSpace || tempBoolPoint
                            || tempQuotationMark || tempDoubleQuotationMark) // Из допустимых все символы, что были заданы выше в bool-ах
                        {
                            allIsOkHere = true; // Всё норм
                        }
                        else
                        {
                            allIsOkHere = false; // "Ой всё" ©
                            break;
                        }
                        break;
                    case "NumbersWithOneComma":
                        if (tempBoolComma) { symbolFoundTwice++; } // Символ должен быть обнаружен не более одного раза
                        if (symbolFoundTwice >= 2)
                        {
                            allIsOkHere = false;
                            symbolFoundTwice = 0;
                            break;
                        }
                        if (tempBoolNumbers || tempBoolComma) // Из допустимых только цифры и запятая
                        {
                            allIsOkHere = true;
                        }
                        else
                        {
                            allIsOkHere = false;
                            symbolFoundTwice = 0;
                            break;
                        }
                        break;
                    case "NumbersWithOneColon":
                        if (tempBoolColon) { symbolFoundTwice++; }
                        if (symbolFoundTwice >= 2)
                        {
                            allIsOkHere = false;
                            symbolFoundTwice = 0;
                            break;
                        }
                        if (tempBoolNumbers || tempBoolColon) // Из допустимых только цифры и двоеточие
                        {
                            allIsOkHere = true;
                        }
                        else
                        {
                            allIsOkHere = false;
                            symbolFoundTwice = 0;
                            break;
                        }
                        break;
                    case "Date":
                        if (tempBoolPoint) { symbolFoundTwice++; }
                        if (symbolFoundTwice >= 3)
                        {
                            allIsOkHere = false;
                            symbolFoundTwice = 0;
                            break;
                        }
                        if (tempBoolNumbers || tempBoolPoint) // Из допустимых только цифры и две точки
                        {
                            allIsOkHere = true;
                        }
                        else
                        {
                            allIsOkHere = false;
                            symbolFoundTwice = 0;
                            break;
                        }
                        if (tempCharIsLast) // Если это последний символ в строке, то надо проверить, действительно ли в строке две точки
                        {
                            if (symbolFoundTwice != 2)
                            {
                                allIsOkHere = false;
                                symbolFoundTwice = 0;
                            }
                        }
                        break;
                }
            }
            return allIsOkHere;
        }
        public Boolean dateCheck(string temp)
        {
            bool allIsOk = true;
            int thisMonthDays = 0;
            // Ниже условия для дней:
            // Если год високосный, то дней в феврале 29
            // Если это 1, 3, 5, 7, 8, 10 либо 12 год месяца, то дней 31, иначе 30
            int tempDay = Convert.ToInt32(temp[0].ToString() + temp[1].ToString());
            int tempMonth = Convert.ToInt32(temp[3].ToString() + temp[4].ToString());
            int tempYear = Convert.ToInt32(temp[6].ToString() + temp[7].ToString() + temp[8].ToString() + temp[9].ToString());

            if (!((tempYear >= 1753) && (tempYear <= 9998))) // Винда влёгкую поддерживает такой временной промежуток
            { allIsOk = false; }
            if (!((tempMonth >= 1) && (tempMonth <= 12)))
            { allIsOk = false; }
            if ((tempMonth == 1) || (tempMonth == 3) || (tempMonth == 5) || (tempMonth == 7)
                || (tempMonth == 8) || (tempMonth == 10) || (tempMonth == 12))
            { thisMonthDays = 31; }
            else { thisMonthDays = 30; }
            if (!((tempDay >= 1) && (tempDay <= thisMonthDays)))
            { allIsOk = false; }
            return allIsOk;
        }

        private void radioButton_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void radioButton1_Checked(object sender, RoutedEventArgs e)
        {

        }
    }
}

